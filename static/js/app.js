
				/*
		Start of Vue.js application
		Manages the front-end of the whole website
		*/
		//URL for the api that gets the coin icons
		let CRYPTOCOMPARE_API_URL = "https://min-api.cryptocompare.com/data/top/mktcapfull?limit=100&tsym=USD&API_KEY{dc24eb951aee25ad15748801681171dd3fc8723c80aca5bb3a66f572bec81d5f}"
		let CRYPTOCOMPARE_LOGOS_API_URL = "https://www.cryptocompare.com/"
		const proxyurl = "https://cors-anywhere.herokuapp.com/"

		//URL for the API that gets the price data
		let COINMARKETCAP_API_URL = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=3c82f7a4-f3d9-4144-9696-57cb3b977dd3"

		let UPDATE_INTERVAL = 60 * 100000;
	

		Vue.filter('toCurrency', function (value) {
			if (typeof value !== "number") {
				return value;
			}
			
			var formatterBig = new Intl.NumberFormat('en-US', {
				style: 'currency',
				currency: 'USD',
				minimumFractionDigits: 0

			});
			var formatterSmall = new Intl.NumberFormat('en-US', {
				style: 'currency',
				currency: 'USD',
				minimumFractionDigits: 4
			});
			
			if (value<1){
				return formatterSmall.format(value);
			}else{
				return formatterBig.format(value);
			}
		});
		Vue.filter('round', function(value, decimals) {
			if(!value) {
				value = 0;
			}

			if(!decimals) {
				decimals = 0;
			}

			value = Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals);
			return value;
		});
		
			 
		let app = new Vue({
			el: "#app",
			data: {
				coinsData: [],
				cmcCoinsData: [],
				favorites: []
			},
			

			methods: {

				/*
				* This method loads all coindata from the CryptoCompare API, After getting
				* the symbol of a coin we use that to look up the logo from the other API
				*/
				getCoinsData: function() {
					let self = this;

					axios.get(proxyurl + CRYPTOCOMPARE_API_URL)
						.then((response) => {
							this.coinsData = response.data.Data;
							console.log(response.data.Data)
							

						})
						.catch((err) => {
							this.getCoins();
							console.error(err);
						})
				},

				getCMCData: function() {
					let self = this;

					axios.get(proxyurl + COINMARKETCAP_API_URL)
						.then((response) => {
							this.cmcCoinsData = response.data.data;
							console.log(response.data.data)
							

						})
						.catch((err) => {
							console.error(err);
						})
				},

				
				
				/*
				* Looks up the logo of the given coin by its symbol
				*/
				getCoinImage: function(index) {
					return CRYPTOCOMPARE_LOGOS_API_URL + app.coinsData[index].CoinInfo.ImageUrl;
				},
				getColor: (num) => {
					return num > 0 ? "color:green;" : "color:red;";
				}
			},
			mounted () {
				this.getCoinsData();
				
			},
		});

		setInterval(() => {
			app.getCoinsData();
		}, UPDATE_INTERVAL);


		
