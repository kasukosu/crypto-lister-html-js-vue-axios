'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const authRoutes = require('./routes/auth-routes');
const passportSetup = require('./config/passport-setup');
const mongoose = require('mongoose');
const keys = require('./config/keys');
const cookieSession = require('cookie-session');
const passport = require('passport')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('static'));
app.set('view engine', 'ejs');



app.use(cookieSession({
	maxAge: 24 * 60 * 60 * 1000,
	keys: [keys.session.cookieKey]
}));

//initialize passport
app.use(passport.initialize());
app.use(passport.session());


//connect to mongodb
mongoose.connect(keys.mongodb.dbURI, () => {
	console.log('connected to mongodb');
})



app.use('/auth', authRoutes);

app.get('/', (req, res) => {
	res.render('home');
});

app.listen(3000, () => {
	console.log('app now listening for request on port 3000')
})



/*app.get('/load', (req, res) => {
	var cursor = db.collection('favorites').find({}, {projection:{ _id: 0 }}).toArray((err, result) => {
		res.send(result);
	});
});

// Add new favorite
app.post('/add', (req, res) => {
	console.log(req.body.todo);
  	db.collection('favorites').save({todo: req.body.todo, done: false}, (err, result) => {
    if (err) return console.log(err)
    	res.send('Added to DB');
  });
})

// Delete todo
app.post('/delete', (req, res) => {
	let task = req.body.todo;
	console.log(task);
	db.collection('todos').findOneAndDelete({todo: task}, (err, result) => {
    if (err) return console.log(err) 
    	res.send('Deleted from DB');
  })
})
*/




